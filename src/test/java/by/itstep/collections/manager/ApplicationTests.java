package by.itstep.collections.manager;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.repository.CollectionEntityRepository;
import by.itstep.collections.manager.repository.CollectionEntityRepositoryImpl;
import by.itstep.collections.manager.repository.CollectionItemEntityRepository;
import by.itstep.collections.manager.repository.CollectionItemEntityRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class ApplicationTests {

    private CollectionEntityRepository collectionRepository = new CollectionEntityRepositoryImpl();
    private CollectionItemEntityRepository itemEntityRepository = new CollectionItemEntityRepositoryImpl();


    @BeforeEach
    void setUp() {
        collectionRepository.deleteAll();
        itemEntityRepository.deleteAll();
    }

    @Test
    void testCreate() {

        //given
        Collection collection = Collection.builder().description("my-description")
                .imageUrl("my-image")
                .title("my-title")
                .name("my-name")
                .build();

        //when

        Collection saved = collectionRepository.create(collection);
        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindAll() {
        //given
        Collection collection = Collection.builder().description("my-description")
                .imageUrl("my-image")
                .title("my-title")
                .name("my-name")
                .build();

        Collection collection2 = Collection.builder().description("my-description")
                .imageUrl("my-image")
                .title("my-title")
                .name("my-name")
                .build();

        collectionRepository.create(collection);
        collectionRepository.create(collection2);

        //when
        List<Collection> list = collectionRepository.findAll();

        //then
        Assertions.assertEquals(2, list.size());


    }

    @Test
    void save_collectionWithoutItems() {

        //Given
        Collection c = Collection.builder()
                .name("name")
                .title("title")
                .imageUrl("imageUrl")
                .description("description")
                .build();

        //When
        Collection saved = collectionRepository.create(c);

        //Then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void save_collectionWithItems() {

        //Given
        Collection c = Collection.builder()
                .name("name")
                .title("title")
                .imageUrl("imageUrl")
                .description("description")
                .build();


        Collection saved = collectionRepository.create(c);


        CollectionItem i1 = CollectionItem.builder().name("item 1").collection(saved).build();
        CollectionItem i2 = CollectionItem.builder().name("item 2").collection(saved).build();

        CollectionItem savedI1 = itemEntityRepository.create(i1);
        CollectionItem savedI2 = itemEntityRepository.create(i2);
        List<CollectionItem> items = new ArrayList<>();
        items.add(savedI1);
        items.add(savedI2);


        //Then
        Assertions.assertNotNull(saved.getId());

    }

    @Test
    void findById_happyPath(){
        //Given
        Collection c = Collection.builder()
                .name("name")
                .title("title")
                .imageUrl("imageUrl")
                .description("description")
                .build();


        Collection saved = collectionRepository.create(c);


        CollectionItem i1 = CollectionItem.builder().name("item 1").collection(saved).build();
        CollectionItem i2 = CollectionItem.builder().name("item 2").collection(saved).build();

        itemEntityRepository.create(i1);
        itemEntityRepository.create(i2);

        //When
        Collection found = collectionRepository.findById(saved.getId());

        //Then

        Assertions.assertNotNull(found);
        Assertions.assertNotNull(found.getId());
        Assertions.assertNotNull(found.getItems());
        Assertions.assertEquals(2,found.getItems().size());
    }
}
