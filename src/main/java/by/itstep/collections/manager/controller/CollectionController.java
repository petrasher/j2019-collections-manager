package by.itstep.collections.manager.controller;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserLoginDto;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.service.AuthService;
import by.itstep.collections.manager.service.CollectionService;
import by.itstep.collections.manager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Arrays;
import java.util.List;

@Controller
public class CollectionController {


    //API:
    //1.Получить главную страницу
    //2.Получить страницу по ID коллекции
    //3.Получить страницу с формой создания коллекции
    //4.Дать возможность создать коллекцию из формы

    @Autowired//-> Spring Пожалуйста заполни это поле
    private CollectionService collectionService ;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthService authService;

    @RequestMapping(method = RequestMethod.GET, value = "/index")// -> localHost:8090/index
    public String openMainPage(Model model) {
        List<CollectionPreviewDto> found = collectionService.findAll();// <- Instead database
        List<String> strings = Arrays.asList("a", "b", "c");
        model.addAttribute("value", "Hello from Thymleaf");
        model.addAttribute("all_collections", found);
        //model.addAttribute("activeUserId", authService.getLoginedUser().getId());
        model.addAttribute("logined",authService.getLoginedUser() != null);
        return "index";//using ThymeLeaf -> resources/tempLates/index.html
    }


    @RequestMapping(method = RequestMethod.GET, value = "/profile/{id}")
    public String openProfile(@PathVariable Long id, Model model) {
        if(!authService.isAuthenticated()){
            return "redirect:/registration";// если ктото не залогинился и ломится в профиль
        }
        if(!authService.getLoginedUser().getId().equals(id)){
            return "redirect:/index";//если ктото ломится в чужой профиль
        }
        UserFullDto found = userService.findById(id);
        model.addAttribute("user", found);
        model.addAttribute("collectionCreateDto", new CollectionCreateDto());

        return "profile";


    }

    //метод нужен чтобы закрыть форму
    @RequestMapping(method = RequestMethod.GET, value = "/registration") //-localhost:8090/registration
    public String openRegistrationPage(Model model) {
        model.addAttribute("createDto", new UserCreateDto());
        model.addAttribute("loginDto", new UserLoginDto());
        return "sing-in";
    }

    //метод нужен для того чтобы принять запрос на сохронение из формы
    @RequestMapping(method = RequestMethod.POST, value = "/user/create")// value из формы
    public String saveUser(UserCreateDto createDto) {
        System.out.println("TO SAVE " + createDto);
        UserFullDto created = userService.create(createDto);
        return "redirect:/registration"; //-localhost:8090/profile/13 -> openProfile()
    }

    @RequestMapping(method = RequestMethod.POST, value = "/user/login")
    public String login(UserLoginDto loginDto){
        authService.login(loginDto);
        // TODO
        return "redirect:/profile/" + authService.getLoginedUser().getId();// TODO
    }

    @RequestMapping(method = RequestMethod.POST, value =  "/collections/create")
    public  String saveCollection(CollectionCreateDto createDto){
        try{
            collectionService.create(createDto);
        }catch (InvalidDtoException e){
            e.printStackTrace();
            return "redirect:/oops";// страница с ошибкой!
        }

        return "redirect:/profile/" + authService.getLoginedUser().getId();
    }

    @RequestMapping(method = RequestMethod.GET , value = "/oops")
    public  String showErrorPage(){
        return "funny-error"; //resourses/teamplates/funny-error.html
    }

    @RequestMapping(method = RequestMethod.GET,value = "/logout")
    public String logout(){
        authService.logOut();
        return "redirect:/index";
    }

}