package by.itstep.collections.manager.entity.enums;

public enum Role {

    ADMIN, USER
}
