package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Tag;

import java.util.List;

public interface TagEntityRepository {

    List<Tag> findAll();


    Tag findById(Long id);


    Tag create (Tag tag);


    Tag update (Tag tag);


    void deleteById(Long id);
}
