package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
@Repository
public class CommentEntityRepositoryImpl implements CommentEntityRepository {
    @Override
    public List<Comment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Comment> foundList = em.createNativeQuery("SELECT * FROM comment", Comment.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + "comment");
        return foundList;
    }

    @Override
    public Comment findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Comment foundComment = em.find(Comment.class, id);

        em.close();
        System.out.println("Found Comment: " + foundComment);
        return foundComment;
    }

    @Override
    public Comment create(Comment comment) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();
        em.close();
        System.out.println("comment was created . id: " + comment.getId());
        return comment;
    }

    @Override
    public Comment update(Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();
        em.close();
        System.out.println("comment was updated. id: " + comment.getId());
        return comment;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Comment foundComment = em.find(Comment.class, id);
        em.remove(foundComment);

        em.getTransaction().commit();
        em.close();

    }
}
