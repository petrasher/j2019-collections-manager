package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Comment;

import java.util.List;

public interface CommentEntityRepository {

    List<Comment> findAll();


    Comment findById(Long id);


    Comment create (Comment comment);


    Comment update (Comment comment);


    void deleteById(Long id);
}
