package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserEntityRepositoryImpl implements UserEntityRepository {

    @Override
    public User findByEmail(String email) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        User foundUser = (User) em.createNativeQuery(
                String.format("SELECT * FROM user WHERE email = \"%s\"", email), User.class)
                .getSingleResult();

        em.close();

        System.out.println("Found: " + foundUser);
        return foundUser;
    }

    @Override
    public List<User> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<User> foundList = em.createNativeQuery("SELECT * FROM user", User.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + "tag");
        return foundList;
    }

    @Override
    public User findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        User foundUser = em.find(User.class, id);

        em.close();
        System.out.println("Found Comment: " + foundUser);
        return foundUser;
    }

    @Override
    public User create(User user) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(user);

        em.getTransaction().commit();
        em.close();
        System.out.println("user was created . id: " + user.getId());
        return user;
    }

    @Override
    public User update(User user) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(user);

        em.getTransaction().commit();
        em.close();
        System.out.println("user was updated. id: " + user.getId());
        return user;

    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        User foundUser = em.find(User.class, id);
        em.remove(foundUser);

        em.getTransaction().commit();
        em.close();

    }
}
