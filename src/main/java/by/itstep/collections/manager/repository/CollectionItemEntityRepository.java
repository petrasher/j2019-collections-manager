package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.dto.collectionItem.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemFullDto;
import by.itstep.collections.manager.entity.CollectionItem;

import java.util.List;

public interface CollectionItemEntityRepository {

    List<CollectionItem> findAll();


    CollectionItem findById(Long id);


    CollectionItem create (CollectionItem collectionItem);


    CollectionItem update (CollectionItem collectionItem);


    void deleteById(Long id);

    void deleteAll();
}
