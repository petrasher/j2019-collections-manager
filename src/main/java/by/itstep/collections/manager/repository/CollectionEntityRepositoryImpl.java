package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
@Repository
public class CollectionEntityRepositoryImpl implements CollectionEntityRepository {
    @Override
    public List<Collection> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List <Collection> foundList = em.createNativeQuery("SELECT * FROM collection", Collection.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " collections");
        return foundList;
    }

    @Override
    public Collection findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        Collection foundCollection = em.find(Collection.class, id);

        //foundCollection.getItems().size();

        Hibernate.initialize(foundCollection.getItems());

        em.close();
        System.out.println("Found collection: " + foundCollection);
        return foundCollection;
    }

    @Override
    public Collection create(Collection collection) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();//Стартуем транзакцию

        //добавление в бд
        em.persist(collection);

        em.getTransaction().commit();// сохраняем все ,что есть
        em.close();
        System.out.println("Collection was created. Id: " + collection.getId());
        return collection;
    }

    @Override
    public Collection update(Collection collection) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();//Стартуем транзакцию

        //добавление в бд
        em.persist(collection);

        em.getTransaction().commit();// сохраняем все ,что есть
        em.close();
        System.out.println("Collection was updated. Id: " + collection.getId());
        return collection;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Collection foundCollection = em.find(Collection.class, id);
        em.remove(foundCollection);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collection").executeUpdate();

        em.getTransaction().commit();
        em.close();


    }
}
