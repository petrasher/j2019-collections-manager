package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
@Repository
public class CollectionItemEntityRepositoryImpl implements CollectionItemEntityRepository {
    @Override
    public List<CollectionItem> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<CollectionItem> foundList = em.createNativeQuery("SELECT * FROM collection_item", CollectionItem.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + "collectionItem");
        return foundList;
    }

    @Override
    public CollectionItem findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        CollectionItem foundCollectionItem = em.find(CollectionItem.class, id);

        em.close();
        System.out.println("Found collectionItem: " + foundCollectionItem);
        return foundCollectionItem;
    }

    @Override
    public CollectionItem create(CollectionItem collectionItem) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(collectionItem);

        em.getTransaction().commit();
        em.close();
        System.out.println("CollectionItem was created . id: " + collectionItem.getId());
        return collectionItem;
    }

    @Override
    public CollectionItem update(CollectionItem collectionItem) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(collectionItem);

        em.getTransaction().commit();
        em.close();
        System.out.println("CollectionItem was updated. id: " + collectionItem.getId());
        return collectionItem;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        CollectionItem foundCollection = em.find(CollectionItem.class, id);
        em.remove(foundCollection);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collection_item").executeUpdate();

        em.getTransaction().commit();
        em.close();


    }
}
