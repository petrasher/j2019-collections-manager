package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
@Repository
public class TagEntityRepositoryImpl implements TagEntityRepository {
    @Override
    public List<Tag> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Tag> foundList = em.createNativeQuery("SELECT * FROM tag", Tag.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + "tag");
        return foundList;
    }

    @Override
    public Tag findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Tag foundTag = em.find(Tag.class, id);

        em.close();
        System.out.println("Found Comment: " + foundTag);
        return foundTag;

    }

    @Override
    public Tag create(Tag tag) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(tag);

        em.getTransaction().commit();
        em.close();
        System.out.println("tag was created . id: " + tag.getId());
        return tag;

    }

    @Override
    public Tag update(Tag tag) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(tag);

        em.getTransaction().commit();
        em.close();
        System.out.println("tag was updated. id: " + tag.getId());
        return tag;

    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Tag foundTag = em.find(Tag.class, id);
        em.remove(foundTag);

        em.getTransaction().commit();
        em.close();

    }
}
