package by.itstep.collections.manager.exception;

public class InvalidDtoException extends  Exception {

    public InvalidDtoException (String message) {
        super ("DANGER: " + message);
    }
}
