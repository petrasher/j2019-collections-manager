package by.itstep.collections.manager.exception;

public class MissedUpdateIdException extends InvalidDtoException {

    public MissedUpdateIdException(String message){
        super(message);
    }

}
