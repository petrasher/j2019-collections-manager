package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collectionItem.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.mapper.CollectionItemMapper;
import by.itstep.collections.manager.repository.CollectionItemEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CollectionItemServiceImpl implements CollectionItemService {

    @Autowired
    private CollectionItemEntityRepository collectionItemEntityRepository;
    @Autowired
    private CollectionItemMapper mapper;



    @Override
    public List<CollectionItemPreviewDto> findAll() {
        List<CollectionItem> found = collectionItemEntityRepository.findAll();

        return mapper.mapToDoList(found);
    }

    @Override
    public CollectionItemFullDto findById(Long id) {
        CollectionItem found = collectionItemEntityRepository.findById(id);

        CollectionItemFullDto responce = mapper.mapTodto(found);
        return responce;
    }

    @Override
    public CollectionItemFullDto create(CollectionItemCreateDto createDto) {
        CollectionItem toSave = mapper.mapToEntity(createDto);

        CollectionItem created = collectionItemEntityRepository.create(toSave);

        return mapper.mapTodto(created);
    }

    @Override
    public CollectionItemFullDto update(CollectionItemUpdateDto updateDto) {
        CollectionItem toUpdate = mapper.mapToEntity(updateDto);

        CollectionItem updated = collectionItemEntityRepository.update(toUpdate);

        return mapper.mapTodto(updated);
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void deleteAll() {

    }
}
