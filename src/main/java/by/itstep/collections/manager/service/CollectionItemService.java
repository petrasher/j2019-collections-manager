package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collectionItem.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.CollectionItem;

import java.util.List;

public interface CollectionItemService {

    List<CollectionItemPreviewDto> findAll();


    CollectionItemFullDto findById(Long id);


    CollectionItemFullDto create (CollectionItemCreateDto createDto);


    CollectionItemFullDto update (CollectionItemUpdateDto updateDto);


    void deleteById(Long id);

    void deleteAll();
}
