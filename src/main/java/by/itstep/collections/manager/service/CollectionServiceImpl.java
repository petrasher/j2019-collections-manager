package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.exception.MissedUpdateIdException;
import by.itstep.collections.manager.mapper.CollectionMapper;
import by.itstep.collections.manager.repository.CollectionEntityRepository;
import by.itstep.collections.manager.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionServiceImpl implements CollectionService {

    @Autowired
    private CollectionEntityRepository collectionRepository;
    @Autowired
    private UserEntityRepository userEntityRepository;
    @Autowired
    private CollectionMapper mapper;


    @Override
    public List<CollectionPreviewDto> findAll() {
        List<Collection> found = collectionRepository.findAll();
        System.out.println("CollectionServiceImpl -> found " + found.size() + " collections");
        return mapper.mapToDtoList(found);
    }

    @Override
    public CollectionFullDto findById(Long id) {
        Collection found = collectionRepository.findById(id);
        System.out.println("CollectionServiceImpl -> found collection " + found);

        CollectionFullDto responce = mapper.mapTodto(found);
        return responce;
    }

    @Override
    public CollectionFullDto create(CollectionCreateDto createDto) throws InvalidDtoException {
        validateCreateDto(createDto);
        User user = userEntityRepository.findById(createDto.getUserId());
        Collection toSave = mapper.mapToEnttity(createDto, user);

        Collection created = collectionRepository.create(toSave);
        System.out.println("CollectionServiceImpl -> created collection " + created);

        CollectionFullDto responce = mapper.mapTodto(created);
        return responce;
    }

    @Override
    public CollectionFullDto update(CollectionUpdateDto updateDto) throws MissedUpdateIdException {
        validateUpdateDto(updateDto);
        Collection entityToUpdate = mapper.mapToEntity(updateDto);
        Collection existingEntity = collectionRepository.findById(updateDto.getId());

        entityToUpdate.setImageUrl(existingEntity.getImageUrl());
        entityToUpdate.setUser(existingEntity.getUser());

        Collection created = collectionRepository.update(entityToUpdate);
        System.out.println("CollectionServiceImpl -> updated collection " + created);

        CollectionFullDto responce = mapper.mapTodto(created);
        return responce;
    }

    @Override
    public void deleteById(Long id) {
        collectionRepository.deleteById(id);
        System.out.println("CollectionServiceImpl ->  collection with id  " + id + "was deleted");

    }

    private void validateCreateDto(CollectionCreateDto dto) throws InvalidDtoException {
        if (dto.getUserId() == null || dto.getDescription() == null ||
                dto.getName() == null || dto.getTitle() == null) {
            throw new InvalidDtoException("One or more fields is NULL");
        }
        if (dto.getDescription().length() < 15){
            throw new InvalidDtoException("Description min length is 15");
        }
    }


    private void validateUpdateDto(CollectionUpdateDto dto) throws MissedUpdateIdException {
        if(dto.getId() == null){
            throw  new MissedUpdateIdException("Collection id is not specified");
        }

    }
}
