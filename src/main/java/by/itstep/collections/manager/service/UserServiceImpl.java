package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.entity.enums.Role;
import by.itstep.collections.manager.mapper.UserMapper;
import by.itstep.collections.manager.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserEntityRepository userRepository;
    @Autowired
    private UserMapper mapper;

    @Override
    public List<UserPreviewDto> findAll() {
        List<User> found = userRepository.findAll();
        return mapper.mapToDtoList(found);
    }

    @Override
    public UserFullDto findById(Long id) {
        User found = userRepository.findById(id);

        UserFullDto responce = mapper.mapToDto(found);
        return responce;
    }

    @Override
    public UserFullDto create(UserCreateDto createDto) {
        User toSave = mapper.mapToEntity(createDto);
        toSave.setRole(Role.USER);
        User created = userRepository.create(toSave);

        UserFullDto responce = mapper.mapToDto(created);
        return responce;
    }

    @Override
    public UserFullDto update(UserUpdateDto updateDto) {
        User entityToUpdate = mapper.mapToEntity(updateDto);
        User existingEntity = userRepository.findById(updateDto.getId());

        entityToUpdate.setRole(existingEntity.getRole());
        User created = userRepository.update(entityToUpdate);

        UserFullDto responce = mapper.mapToDto(created);
        return responce;
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);

    }

}
