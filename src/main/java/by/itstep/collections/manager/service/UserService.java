package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;


import java.util.List;

public interface UserService {

    List<UserPreviewDto> findAll();


    UserFullDto findById(Long id);


    UserFullDto create (UserCreateDto createDto);


    UserFullDto update (UserUpdateDto updateDto);


    void deleteById(Long id);
}
