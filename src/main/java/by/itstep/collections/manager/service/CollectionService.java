package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.exception.MissedUpdateIdException;

import java.util.List;

public interface CollectionService {

    List<CollectionPreviewDto> findAll();


    CollectionFullDto findById(Long id);

    CollectionFullDto create (CollectionCreateDto createDto) throws InvalidDtoException;


    CollectionFullDto update (CollectionUpdateDto updateDto) throws MissedUpdateIdException;


    void deleteById(Long id);
}
