package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.user.UserLoginDto;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.entity.enums.Role;
import by.itstep.collections.manager.repository.UserEntityRepository;
import by.itstep.collections.manager.repository.UserEntityRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private User loginedUser;
    @Autowired
    private UserEntityRepository userRepository;


    @Override
    public void login(UserLoginDto loginDto) {
        User foundUser = userRepository.findByEmail(loginDto.getEmail());
        if (foundUser == null) {
            throw new RuntimeException("User not found by email: " + loginDto.getEmail());
        }

        if (!foundUser.getPassword().equals(loginDto.getPassword())) {
            throw new RuntimeException("User password is incorrect");
        }

        loginedUser = foundUser;// сохранили как залогиненного

    }

    @Override
    public void logOut() {
        loginedUser = null;
    }


    @Override
    public boolean isAuthenticated() {
        return loginedUser != null;
    }

    @Override
    public Role getRole() {
        return loginedUser.getRole();

    }

    @Override
    public User getLoginedUser() {
        return loginedUser;
    }


}
