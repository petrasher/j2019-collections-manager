package by.itstep.collections.manager.dto.collection;

import lombok.Data;

@Data
public class CollectionUpdateDto {
    private Long id;

    private String name;

    private String title;

    private String description;

   // private String imageUrl; <- не хотим менять


}
