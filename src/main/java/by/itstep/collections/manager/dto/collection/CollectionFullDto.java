package by.itstep.collections.manager.dto.collection;

import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.entity.User;
import lombok.Data;

import java.util.List;

@Data
public class CollectionFullDto {


    private Long id;

    private String name;

    private String title;

    private String description;

    private String imageUrl;

    private List<CollectionItem> items;

    private List<Comment> comments;

    private User user;

    private List<Tag> tags;
}
