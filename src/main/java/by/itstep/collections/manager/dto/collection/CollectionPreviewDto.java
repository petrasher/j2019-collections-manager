package by.itstep.collections.manager.dto.collection;

import by.itstep.collections.manager.entity.Tag;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectionPreviewDto {

    private Long id;

    private String name;

    private String title;

    private  String imageUrl;

    private String userName;

    private List<Tag> tags;
}
