package by.itstep.collections.manager.dto.collection;

import lombok.Data;

@Data
public class CollectionCreateDto {

    private String name;

    private String title;

    private String description;

    private String imageUrl;

    private Long userId;


}
