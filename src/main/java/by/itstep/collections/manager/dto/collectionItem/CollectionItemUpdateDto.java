package by.itstep.collections.manager.dto.collectionItem;

import by.itstep.collections.manager.entity.Collection;
import lombok.Data;


@Data
public class CollectionItemUpdateDto {

    private Long id;

    private String name;

    private Collection collection;


}
