package by.itstep.collections.manager.dto.user;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.enums.Role;
import lombok.Data;

import java.util.List;

@Data
public class UserFullDto {

    private Long id;

    private String name;

    private String lastName;

    private String email;

    private List<Collection> collections;

    private List<Comment> comments;

    private Role role;

    public UserFullDto() {
    }
}
