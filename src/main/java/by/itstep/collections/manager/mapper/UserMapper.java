package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;
import by.itstep.collections.manager.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class UserMapper {

    public List<UserPreviewDto> mapToDtoList(List<User> entities) {
        List<UserPreviewDto> dtos = new ArrayList<>();
        for (User entity : entities) {

            UserPreviewDto dto = new UserPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setEmail(entity.getEmail());
            dto.setRole(entity.getRole());

            dtos.add(dto);
        }

        return dtos;
    }


    public User mapToEntity(UserCreateDto createDto) {

        User user = new User();

        user.setName(createDto.getName());
        user.setLastName(createDto.getLastName());
        user.setEmail(createDto.getEmail());
        user.setPassword(createDto.getPassword());

        return user;
    }

    public User mapToEntity(UserUpdateDto updateDto) {

        User user = new User();

        user.setId(updateDto.getId());
        user.setName(updateDto.getName());
        user.setLastName(updateDto.getLastName());
        user.setEmail(updateDto.getEmail());
        //user.setRole(updateDto.getRole());

        return user;
    }

    public UserFullDto mapToDto(User entity){
        UserFullDto fullDto = new UserFullDto();
        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setEmail(entity.getEmail());
        fullDto.setCollections(entity.getCollections());
        fullDto.setComments(entity.getComments());
        fullDto.setRole(entity.getRole());

        return  fullDto;
    }
}
