package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collectionItem.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class CollectionItemMapper {

    public List<CollectionItemPreviewDto> mapToDoList(List<CollectionItem> entities) {
        List<CollectionItemPreviewDto> dtos = new ArrayList<>();
        for (CollectionItem entity : entities) {

            CollectionItemPreviewDto dto = new CollectionItemPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setCollection(entity.getCollection());

            dtos.add(dto);
        }

        return dtos;
    }

public CollectionItem mapToEntity(CollectionItemCreateDto createDto){

        CollectionItem collectionItem = new CollectionItem();

        collectionItem.setName(createDto.getName());
        collectionItem.setCollection(createDto.getCollection());

        return collectionItem;
    }

    public CollectionItem mapToEntity (CollectionItemUpdateDto updateDto){

        CollectionItem collectionItem = new CollectionItem();

        collectionItem.setId(updateDto.getId());
        collectionItem.setName(updateDto.getName());
        collectionItem.setCollection(updateDto.getCollection());

        return collectionItem;
    }

    public CollectionItemFullDto mapTodto(CollectionItem collectionItem){
        CollectionItemFullDto fullDto = new CollectionItemFullDto();
        return  fullDto;
    }
}
