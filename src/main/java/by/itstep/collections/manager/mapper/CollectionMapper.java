package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class CollectionMapper {

    public List<CollectionPreviewDto> mapToDtoList(List<Collection> entities) {
        List<CollectionPreviewDto> dtos = new ArrayList<>();
        for (Collection entity : entities) {
            //1.создать новый dto
            CollectionPreviewDto dto = new CollectionPreviewDto();
            //.перелить в него данный из entity
            dto.setId(entity.getId());
            dto.setImageUrl(entity.getImageUrl());
            dto.setName(entity.getName());
            dto.setTags(entity.getTags());
            dto.setTitle(entity.getTitle());
            dto.setUserName(entity.getUser().getName() + " " + entity.getUser().getLastName());
            //3. Добавить полученный dto в список
            dtos.add(dto);
        }

        return dtos;
    }

    public Collection mapToEnttity(CollectionCreateDto createDto, User user){
        Collection collection = new Collection();
        collection.setDescription(createDto.getDescription());
        collection.setName(createDto.getName());
        collection.setTitle(createDto.getTitle());
        collection.setImageUrl(createDto.getImageUrl());
        collection.setUser(user);

        return collection;
    }

    public Collection mapToEntity(CollectionUpdateDto updateDto) {

        Collection entity = new Collection();
        entity.setId(updateDto.getId());
        entity.setName(updateDto.getName());
        entity.setTitle(updateDto.getTitle());
        entity.setDescription(updateDto.getDescription());
       // entity.setImageUrl(updateDto.getImageUrl());

        return entity;
    }

    public CollectionFullDto mapTodto(Collection entity){
        CollectionFullDto fullDto = new CollectionFullDto();

        return fullDto;
    }
}
